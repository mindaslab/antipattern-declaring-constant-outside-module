require_relative 'module_a'
require_relative 'module_b'

include ModuleA
include ModuleB

p ModuleA::SOME_CONSTANT
p ModuleB::SOME_CONSTANT
p SOME_CONSTANT
p ModuleA::SOME_CONSTANT
